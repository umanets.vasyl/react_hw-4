import React from "react";
import Products from "../../components/Products/Products";
import './CartPage.scss'



const CartPage = ({
  routeProducts,
  products,
  favorites,
  onAddFavFavourite,
  onAdd,
  modalCart,
}) => {
  return (
    <div className="container">
      {routeProducts.length > 0 ? (
        <>
          <h3 className="cart-container__title">
            В корзину додано {routeProducts.length}{" "}
            {routeProducts.length > 4
              ? "товарів"
              : `${routeProducts.length > 1 ? "товари" : "товар"}`}
          </h3>

          <Products
            routeProducts={routeProducts}
            products={products}
            favorites={favorites}
            onAddFavourite={onAddFavFavourite}
            onAdd={onAdd}
            action={modalCart}
            text="Видалити з корзини"
            btnClassName="btn-delete"
          />
        </>
      ) : (
        <div className="empty">
          <h3 className="epmty-cart">КОРЗИНА ПУСТА!</h3>
        </div>
      )}
    </div>
  );
};

export default CartPage;