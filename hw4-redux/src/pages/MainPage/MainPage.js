import Products from "../../components/Products/Products";

function MainPage({
  routeProducts,
  products,
  favorites,
  onAddFavFavourite,
  onAdd,
  modalCart,
}) {
  return (
    <div className={"main"}>
      <Products
        routeProducts={routeProducts}
        products={products}
        favorites={favorites}
        onAddFavourite={onAddFavFavourite}
        onAdd={onAdd}
        modalCart={modalCart}
        action={modalCart}
        text={"Додати до корзини"}
        btnClassName="card__footer-button"
      />
    </div>
  );
}

export default MainPage