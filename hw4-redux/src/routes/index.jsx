import { Navigate, Route, Routes } from "react-router-dom";
import Layout from "../components/Layout/Layout";
import MainPage from "../pages/MainPage/MainPage";
import FavoritesPage from "../pages/FavoritesPage/FavoritesPage";
import CartPage from "../pages/CartPage/CartPage";

function Router({
  productsInCart,
  favorites,
  products,
  addToFavourite,
  openModalAddToCart,
  modalCart,
  openModalDeleteFromCart,
}) {
  return (
    <Routes>
      <Route
        path="/"
        element={
          <Layout
            cartLength={productsInCart.length}
            favLength={favorites.length}
          />
        }
      >
        <Route index element={<Navigate to={"/main"} />} />
        <Route
          path="/main"
          element={
            <MainPage
              routeProducts={products}
              products={products}
              favorites={favorites}
              onAddFavFavourite={addToFavourite}
              onAdd={openModalAddToCart}
              modalCart={modalCart}
            />
          }
        />
        <Route
          path="/favorites"
          element={
            <FavoritesPage
              routeProducts={favorites}
              products={products}
              favorites={favorites}
              onAddFavFavourite={addToFavourite}
              onAdd={openModalAddToCart}
              modalCart={modalCart}
            />
          }
        />
        <Route
          path="/cart"
          element={
            <CartPage
              routeProducts={productsInCart}
              products={products}
              favorites={favorites}
              onAddFavFavourite={addToFavourite}
              onAdd={openModalDeleteFromCart}
              modalCart={modalCart}
            />
          }
        />
      </Route>
    </Routes>
  );
}

export default Router;
