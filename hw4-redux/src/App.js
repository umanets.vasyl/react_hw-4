import React, { useEffect, useState } from "react";
import Modal from "./components/Modal/Modal";
import Router from "./routes";
import { useDispatch, useSelector } from "react-redux";
import {
  showModalAddToCart,
  loadAllProducts,
  hideModalAddToCart,
  showModalDeleteFromCart,
  hideModalDeleteFromCart,
} from "./store/actions";

import "./App.scss";

function App() {
  const [favorites, setFavorites] = useState([]);
  const [productsInCart, setProductsInCart] = useState([]);
  const [itemToBeAdded, setItemToBeAdded] = useState(null);
  const [itemToBeRemove, setItemToBeRemove] = useState(null);

  const dispatch = useDispatch();

  const { products, modalCart, deleteCartModal } = useSelector((state) => ({
    products: state.productsReducer.products,
    modalCart: state.cartModalReducer.modalCart,
    deleteCartModal: state.cartModalReducer.deleteCartModal,
  }));

  const initStorageData = (products, itmprop) => {
    const storedInCart = JSON.parse(localStorage.getItem(itmprop)) || [];
    const productsArr = [];
    for (const id of storedInCart) {
      const product = products.find((i) => i.articul === id);
      productsArr.push(product);
    }
    switch (itmprop) {
      case "productsInCart":
        setProductsInCart([...productsInCart, ...productsArr]);
        break;
      case "favorites":
        setFavorites([...favorites, ...productsArr]);
        break;
    }
  };

  useEffect(() => {
    dispatch(loadAllProducts());
    initStorageData(products, "productsInCart");
    initStorageData(products, "favorites");
  }, []);

  function addToFavourite(product) {
    let fav = [];
    let findElement = favorites.find(
      (elem) => elem.articul === product.articul
    );
    if (findElement) {
      fav = favorites.filter((elem) => elem.articul !== product.articul);
    } else {
      fav = [...favorites, product];
    }
    setFavorites(fav);
    localStorage.setItem(
      "favorites",
      JSON.stringify(fav.map((elem) => elem.articul))
    );
  }

  const addToCart = () => {
    const cartItem = [...productsInCart, itemToBeAdded];
    setProductsInCart(cartItem);
    dispatch(hideModalAddToCart());
    localStorage.setItem(
      "productsInCart",
      JSON.stringify(cartItem.map((elem) => elem.articul))
    );
  };

  const openModalAddToCart = (product) => {
    
    setItemToBeAdded(product);
    console.log("added", product.id);
    dispatch(showModalAddToCart());
  };
  const openModalDeleteFromCart = (product) => {
    setItemToBeRemove(product);
    dispatch(showModalDeleteFromCart());
  };

  const removeFromCart = () => {
    const filteredItems = productsInCart.filter(
      (i) => i.articul !== itemToBeRemove.articul
    );
    setProductsInCart(filteredItems);
    dispatch(hideModalDeleteFromCart());
    localStorage.setItem("productsInCart", JSON.stringify(filteredItems));
  };
  return (
    <>
      {deleteCartModal && (
        <Modal
          modalTitle={"Видалити товар"}
          modalText={"Чи хочете видалити товар з корзини?"}
          closeModal={hideModalDeleteFromCart}
          submitFunction={removeFromCart}
        />
      )}

      {modalCart && (
        <Modal
          modalTitle={"Додати до корзини"}
          modalText={"Чи хочете додати товар до корзини?"}
          closeModal={hideModalAddToCart}
          submitFunction={addToCart}
        />
      )}

      <Router
        productsInCart={productsInCart}
        favorites={favorites}
        products={products}
        addToFavourite={addToFavourite}
        openModalAddToCart={openModalAddToCart}
        modalCart={modalCart}
        openModalDeleteFromCart={openModalDeleteFromCart}
      />
    </>
  );
}

export default App;
