export const allProducts = "ALL_PRODUCTS";
export const OpenModalAddToCart = "OpenModalAddToCart";
export const CloseModalAddToCart = "CloseModalAddToCart";
export const OpenModalDeleteFromCart = "OpenModalDeleteFromCart";
export const CloseModalDeleteFromCart = "CloseModalDeleteFromCart";
