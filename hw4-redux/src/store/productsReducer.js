import { allProducts } from "./constants";

const initialState = {
  products: [],
};
export default function productsReducer(state = initialState, action) {
  switch (action.type) {
    case allProducts:
      return { ...state, products: action.payload };
    default:
      return state;
  }
}
