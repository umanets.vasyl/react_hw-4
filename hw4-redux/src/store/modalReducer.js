import {  
  OpenModalAddToCart,
  CloseModalAddToCart,
  OpenModalDeleteFromCart,
  CloseModalDeleteFromCart,
} from "./constants";

const initialState = {
  modalCart: false,
  deleteCartModal: false,
};

export default function cartModalReducer(state = initialState, action) {
  switch (action.type) {
    case OpenModalAddToCart:
      return { ...state, modalCart: action.payload };
    case CloseModalAddToCart:
      return { ...state, modalCart: action.payload };
    case OpenModalDeleteFromCart:
      return { ...state, deleteCartModal: action.payload };
    case CloseModalDeleteFromCart:
      return { ...state, deleteCartModal: action.payload };
    default:
      return state;
  }
}
