import {allProducts, OpenModalAddToCart, CloseModalAddToCart, OpenModalDeleteFromCart,CloseModalDeleteFromCart} from './constants'

export const loadAllProducts = () => {
  return async (dispatch) => {
    const res = await fetch("/products.json");
    console.log(allProducts);
    dispatch({ type: allProducts, payload: await res.json() });
  };
};
export const showModalAddToCart = () => {
  return (dispatch) => {
    dispatch({ type: OpenModalAddToCart, payload: true });
  };
};
export const hideModalAddToCart = () => {
  return (dispatch) => {
    dispatch({ type: CloseModalAddToCart, payload: false });
  };
};

export const showModalDeleteFromCart = () => {
  return (dispatch) => {
    dispatch({ type: OpenModalDeleteFromCart, payload: true });
  };
};

export const hideModalDeleteFromCart = () => {
  return (dispatch) => {
    dispatch({ type: CloseModalDeleteFromCart, payload: false });
  };
};
