import { Outlet } from "react-router-dom";
import React from "react";

import Header from "../Header/Header";

const Layout = ({ cartLength, favLength }) => {
  return (
    <>
      <Header cartLength={cartLength} favLength={favLength} />
      <Outlet />
    </>
  );
};

export default Layout;
