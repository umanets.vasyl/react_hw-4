import React from "react";
import "./Products.scss";
import ProductCard from "./ProductCard";
import Button from "../Button/Button";

const Products = ({
  routeProducts,
  favorites,
  onAddFavourite,
  onAdd,
  text,
  btnClassName,
}) => {
  const product = routeProducts.map((item) => (
    <ProductCard
      key={item.articul}
      product={item}
      favorites={favorites}
      onAdd={onAdd}
      onAddFavourite={onAddFavourite}
      action={
        <Button
          clickHandler={() => onAdd(item)}
          text={text}
          className={btnClassName}
        />
      }
    />
  ));
  return (
    <div className="products">
      <div className="products__container">
        <h2 className="products__title">ТЕЛЕВІЗОРИ</h2>
        <div className="products__wrap">{product}</div>
      </div>
    </div>
  );
};

export default Products;
