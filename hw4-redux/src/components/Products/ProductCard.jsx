import React, { useMemo } from "react";
import propTypes from "prop-types";
import "./ProductCard.scss";

function ProductCard({ favorites, product, action, onAddFavourite }) {
  const isFavoutite = useMemo(
    () => !!favorites.find((e) => e.articul === product.articul),
    [favorites, product]
  );

  return (
    <div className="card">
      <div className="card__header">
        <div
          className="favourite__icon-wrapper"
          onClick={() => {
            onAddFavourite(product);
          }}
        >
          <img
            className="favourite__icon"
            src={
              isFavoutite ? "/img/icons/star-active.png" : "/img/icons/star.png"
            }
            alt="star"
          />
        </div>
        <img className="card__header-img" src={product.img_url} alt="img" />
      </div>
      <div className="card__content">
        <div className="card__content-articul">Артикул: {product.articul}</div>
        <div className="card__content-title">Телевізор {product.name}</div>
        <div className="card__content-color">Колір: {product.color}</div>
      </div>
      <div className="card__footer">
        <div className="card__footer-price">{product.price}грн</div>
        {action}
      </div>
    </div>
  );
}

export default ProductCard;

ProductCard.propTypes = {
  articul: propTypes.number,
  name: propTypes.string,
  price: propTypes.number,
  color: propTypes.string,
  img_url: propTypes.string,
  onAdd: propTypes.func,
};
