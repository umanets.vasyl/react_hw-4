import "./Favourite.scss";

const Favourite = ({ favCount }) => {
  return (
    <div className="favourite__wrap">
      <img
        className="favourite__wrap-img"
        src="/img/icons/star-active.png"
        alt="star"
      />
      <p className="favourite__count">Вибрані ({favCount})</p>
    </div>
  );
};

export default Favourite;