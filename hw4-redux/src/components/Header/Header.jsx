import { NavLink } from "react-router-dom";
import "../Header/Header.scss";
import Cart from "./Cart";
import Favourite from "./Favourite";

const Header = ({ favLength, cartLength }) => {
  return (
    <header className="header">
      <div className="header__container">
        <NavLink to="main">
          <img
            className="site__logo"
            src="./img/icons/televection-logo.png"
            alt="logo"
          />
        </NavLink>
        <div className="header-wrap">
          <div className="header-right">
            <NavLink to="favorites">
              <Favourite favCount={favLength} />
            </NavLink>
            <NavLink to="cart">
              <Cart cartCount={cartLength} />
            </NavLink>
          </div>
        </div>
      </div>
    </header>
  );
};

export default Header;
