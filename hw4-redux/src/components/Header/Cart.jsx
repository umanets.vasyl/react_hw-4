import "./Cart.scss";
const Cart = ({ cartCount }) => {
  return (
    <div className="cart">
      <img
        className="cart__icon"
        src="./img/icons/red-shopping-cart-10906.png"
        alt="cart"
      />
      <p className="cart__count">Корзина ({cartCount})</p>
    </div>
  );
};

export default Cart;
